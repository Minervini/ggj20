﻿using UnityEngine;
using System.Collections;

public class PlayerBrick : MonoBehaviour
{
    public bool isLowerPlayer;
    public Transform hand;
    public bool isHoldingBrick = false;
    public GameObject brick;
    public float ThrowForce;
    public ConstantForce2D wind;
    public float windForce = 1f;

    public GameObject house;

    private void Start()
    {
        World.OnWindChanged += OnWindChanged;
    }

    private void OnWindChanged(Vector2 force)
    {
        wind.force = new Vector2(force.x * 5f, 0f);
    }

    public IEnumerator waiter()
    {
        yield return new WaitForSeconds(0.2f);
    }

    public void GrabBrick(GameObject brick)
    {
        if(!isHoldingBrick && brick.GetComponent<Brick>().Grabbable) {
            brick.GetComponent<Brick>().SignalGrab();
            brick.transform.localEulerAngles = new Vector3(0, 0, 0);
            brick.transform.SetParent(hand);
            brick.transform.position = hand.position;
            brick.GetComponentInChildren<Interactable>().SetActive(false);
            this.brick = brick;
            isHoldingBrick = true;
            brick.GetComponent<SpriteRenderer>().sortingOrder = 1509;
        }
    }

    public void ThrowBrick(Vector3 force) {
        if(isHoldingBrick && isLowerPlayer) {
            isHoldingBrick = false;
            brick.transform.SetParent(null);
            brick.GetComponentInChildren<Interactable>().SetActive(true);
            brick.GetComponent<Brick>().Throw(force * ThrowForce);
        }
    }

    public void FixHole(GameObject hole) {
        if(isHoldingBrick && !isLowerPlayer) {
            isHoldingBrick = false;
            Destroy(brick);
            brick = null;
            house.GetComponent<House>().repareTile(hole);
        }
    }

    private void Update() {
        if(isHoldingBrick) {
            Debug.DrawLine(Camera.main.ScreenToWorldPoint(Input.mousePosition), transform.position);
        }
        if(Input.GetAxisRaw("Fire1") == 1) {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            Vector3 force = new Vector3(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y, 0);
            ThrowBrick(force);
        }
    }
}
