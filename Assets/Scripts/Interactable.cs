﻿using UnityEngine;

public class Interactable : MonoBehaviour
{
    
    public enum Type{Brick = 1, Hole = 2}

    public Type type;
    private bool isActive = true;

    private static float pressedTime;


    public void SetActive(bool active)
    { 
        isActive = active;

        if(active) {
            transform.parent.gameObject.AddComponent<Rigidbody2D>();
            transform.parent.gameObject.AddComponent<ConstantForce2D>();
        } else {
            Destroy(transform.parent.gameObject.GetComponent<ConstantForce2D>());
            Destroy(transform.parent.gameObject.GetComponent<Rigidbody2D>());
        }

        //transform.parent.gameObject.GetComponent<BoxCollider2D>().enabled = active;
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "LowerPlayer" && Input.GetAxisRaw("Interact") == 1 && isActive)
        {
            if (type == Type.Brick)
            {
                other.gameObject.GetComponent<PlayerBrick>().GrabBrick(transform.parent.gameObject);
            }
        }

        else if (other.gameObject.tag == "UpperPlayer" && Input.GetAxisRaw("Interact") == -1 && isActive)
        {
            if (type == Type.Brick)
            {
                other.gameObject.GetComponent<PlayerBrick>().GrabBrick(transform.parent.gameObject);
                pressedTime = Time.time;
            }
            else if (type == Type.Hole)
            {
                if (Time.time - pressedTime > 0.2f)
                {
                    other.gameObject.GetComponent<PlayerBrick>().FixHole(gameObject);
                }
            }
        }
    }
}
