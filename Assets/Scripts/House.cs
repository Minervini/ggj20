﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour
{
    public GameObject tilesPlacesContainer;
    public int startingTilesNumber = 68;
    public GameObject holePrefab;
    public GameObject tuile;

    public GameObject roofDown;

    public int count;

    private List<GameObject> tilePlaces;

    private void Awake()
    {
        if (startingTilesNumber > tilesPlacesContainer.transform.childCount)
        {
            startingTilesNumber = tilesPlacesContainer.transform.childCount - 1;
        }
        if (startingTilesNumber < 1)
        {
            startingTilesNumber = 1;
        }

        count = tilesPlacesContainer.transform.childCount;

        tilePlaces = new List<GameObject>();
        foreach (Transform placeTransform in tilesPlacesContainer.transform)
        {
            GameObject tile = placeTransform.GetChild(0).gameObject;

            tilePlaces.Add(placeTransform.gameObject);
            tile.GetComponent<Brick>().Disable();
        }

        while (count > startingTilesNumber)
        {
            RemoveRandomTile();
        }
    }

    void OnCollisionEnter2D(Collision2D collider2D)
    {
        if (collider2D.gameObject.tag == "Cow") {
            RemoveRandomTile();
        }
    }

    public bool repareTile(GameObject hole) {
        GameObject parent = hole.transform.parent.gameObject;
        if (hole.name != "Hole")
            return false;
        Destroy(hole);
        count++;
        Debug.Log("Ca MarAEANFJNFVPQKNVKOPAENFEQOJKNFGONVOKJZNVOAFNVOKAZNVAZ");
        var clone = Instantiate(tuile, parent.transform);
        clone.GetComponent<Brick>().Disable();
        return true;
    }

    public void RemoveRandomTile() {
        bool removed = false;
        while (!removed && count > 0) {
            removed = RemoveTile(Random.Range(0, tilePlaces.Count - 1));
        }
            
    }

    public bool RemoveTile(int idx)
    {
        GameObject tilePlace = tilePlaces[idx];
        GameObject tile = tilePlace.transform.GetChild(0).gameObject;
        if (tile.GetComponent<Brick>() == null)
            return false;
        Destroy(tile);
        count--;
        var clone = Instantiate(holePrefab, tilePlace.transform);
        clone.name = "Hole";
        return true;
    }
}
