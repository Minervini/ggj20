﻿using TMPro;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public bool isLowerPlayer;
    public float Multiplier;
    public float JumpMultiplier;
    public float MaxVelocity;
    public float jumpCooldown = 0.1F;
    public bool canJump = true;
    public ConstantForce2D windForce;

    private Animator anim;
    private Quaternion rotation_default = Quaternion.Euler(0, 0, 0);
    private Quaternion rotation_mirrored = Quaternion.Euler(0, 180, 0);
    private Rigidbody2D _rigidbody;
    private float lastJump;
    private bool isAnimated;
    
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody2D>();
        anim.Play("Idle");
    }

    // Update is called once per frame
    void Update()
    {
        var movement = new Vector2(0, 0);
        int isInteracting = 0;

        if(Mathf.Abs(_rigidbody.velocity.x) < MaxVelocity) {
            if (isLowerPlayer) {
                if (Input.GetKey("left")) {
                    isInteracting = 1;
                    movement = new Vector2(-1, 0) * Multiplier;
                    transform.rotation = rotation_default;
                } else if (Input.GetKey("right")) {
                    isInteracting = 1;
                    movement = new Vector2(1, 0) * Multiplier;
                    transform.rotation = rotation_mirrored;
                }
            } else {
                var VsPEED = Input.GetAxisRaw("Vertical");
                var hSpeed = 0;

                if(Input.GetKey("q"))
                {
                    isInteracting = 1;
                    hSpeed = -1;
                    transform.rotation = rotation_default;
                }
                if (Input.GetKey("d"))
                {
                    isInteracting = 1;
                    hSpeed = 1;
                    transform.rotation = rotation_mirrored;
                }
                if (VsPEED > 0f)
                {
                    isInteracting = 1;
                }
                movement = new Vector2(hSpeed, VsPEED) * Multiplier * .5f;
            }
        }
        else
        {
            isInteracting = 1;
        }
        var jump = Input.GetAxisRaw("Jump");

        // Handle jumping
        if ((System.Math.Abs(jump) > .1f) && canJump) {
            canJump = false;
            lastJump = Time.fixedTime;
            movement.y = JumpMultiplier;
        }

        _rigidbody.AddForce(movement);

        if (isInteracting == 1 && !isAnimated)
        {
            anim.Play("Walk");
            isAnimated = true;
        }
        else if (isInteracting == 0 && isAnimated)
        {
            anim.Play("Idle");
            isAnimated = false;
        }
    }

    void OnCollisionEnter2D(Collision2D obj)
    {
        canJump = obj.gameObject.tag == "Ground" && Time.fixedTime >= (lastJump + jumpCooldown);
    }
}
