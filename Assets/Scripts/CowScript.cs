﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CowScript : MonoBehaviour
{
    public int lifeTime;

    // Start is called before the first frame update
    void Start()
    {
        if (lifeTime <= 0) {
            lifeTime = 10;
        }
        StartCoroutine(Live());
    }

    private IEnumerator Live() {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
    }


    private void FixedUpdate()
    {
        if(transform.position.y < -10)
            Destroy(gameObject);
    }
}
