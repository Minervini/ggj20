﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    public bool hasBeenThrowed;
    public bool Grabbable;
    public float lastGrabbed;
    public bool enable;
    public float grabCooldown = 0.5F;

    private Vector2 windForce;

    private void Start()
    {
        hasBeenThrowed = false;
        Grabbable = false;
        enable = true;
        lastGrabbed = Time.fixedTime;
        World.OnWindChanged += OnWindChanged;
    }

    private void FixedUpdate()
    {
        if (GetComponent<Rigidbody2D>() && System.Math.Abs(windForce.magnitude) > .1f)
        {
            GetComponent<ConstantForce2D>().force = windForce;
        }

        if(transform.position.y < -10)
            Destroy(gameObject);
    }

    private void OnWindChanged(Vector2 wind)
    {
        Rigidbody2D body = GetComponent<Rigidbody2D>();

        if (body != null)
        {
            windForce = wind/2.0f;
        }
    }

    public void Update()
    {
        if(enable && Time.fixedTime > lastGrabbed + grabCooldown)
        {
            Grabbable = true;
        }
    }

    public void Throw(Vector3 force)
    {
        Debug.Log(force);
        GetComponent<Rigidbody2D>().AddTorque(20, ForceMode2D.Force);
        GetComponent<Rigidbody2D>().AddForce(force);
        hasBeenThrowed = true;
    }

    public void SignalGrab()
    {
        lastGrabbed = Time.fixedTime;
        Grabbable = false;
    }

    void OnCollisionEnter2D(Collision2D collider2D)
    {
        if(hasBeenThrowed) {
            Destroy(gameObject);
        }
    }

    public void OnDestroy() {
        World.OnWindChanged -= OnWindChanged;
    }

    public void Disable()
    {
        enable = false;
        Grabbable = false;
        Destroy(GetComponent<BoxCollider2D>());
        Destroy(GetComponentInChildren<BoxCollider2D>());
        Destroy(GetComponentInChildren<Interactable>());
        Destroy(GetComponent<ConstantForce2D>());
        Destroy(GetComponent<Rigidbody2D>());   
    }
}
