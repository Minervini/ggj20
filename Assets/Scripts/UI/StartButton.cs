﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartButton : MonoBehaviour
{
    public string SceneName;

    public void StartGame() {
        Debug.Log("Start");
        SceneManager.LoadScene(SceneName, LoadSceneMode.Single);
    }
}
