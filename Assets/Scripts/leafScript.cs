﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class leafScript : MonoBehaviour
{

    public Vector2 wind;
    // Start is called before the first frame update
    void Start()
    {
        wind.x = Random.Range(wind.x / 4, wind.x);
        wind.y = Random.Range(wind.y / 4, wind.y);
        GetComponent<Rigidbody2D>().SetRotation(Random.Range(0, 45));
        GetComponent<Rigidbody2D>().AddForce(wind);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       
        if(transform.position.y < -10)
            Destroy(gameObject);
        if(transform.position.y > 100)
            Destroy(gameObject);
        if(transform.position.x < -50)
            Destroy(gameObject);
        if(transform.position.y > 50)
            Destroy(gameObject);
    }
}
