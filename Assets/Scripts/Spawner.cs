﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Spawner : MonoBehaviour
{
    public Brick prefab;
    public Transform transform1;

    private float cooldown = 3.0F;
    private float lastSpawn = 0F;

    public void Start() {
        transform1 = GetComponent<Transform>();
    }
    
    public void SpawnItem() {
        Instantiate(prefab, new Vector3(transform1.position.x, transform1.position.y, transform1.position.z), Quaternion.identity);
        prefab.Throw(new Vector3(100, -100, 0));
    }

    void OnTriggerStay2D(Collider2D other) {
        if (other.gameObject.tag == "LowerPlayer" && Input.GetAxisRaw("Interact") == 1) {
            if(Time.fixedTime > lastSpawn + cooldown) {
                SpawnItem();
                lastSpawn = Time.fixedTime;
            }

        }
    }
}
