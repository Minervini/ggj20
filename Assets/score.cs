﻿
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.SceneManagement;

public class score : MonoBehaviour
{
    
    public Text text;//is called once per frame
    public House house;
    public string EndSceneName;
    void Start(){
        house = FindObjectOfType<House>();
    }
    void Update()
    {
        text.text = house.count + " / " + house.tilesPlacesContainer.gameObject.transform.childCount;
        if (house.count == house.tilesPlacesContainer.gameObject.transform.childCount) {
            Debug.Log("End");
            SceneManager.LoadScene(EndSceneName, LoadSceneMode.Single);
            FindObjectOfType<TextScript>().changeText("Bien joué ! La maison a pu être réparée !");
        }
        if (house.count == 0) {
            Debug.Log("End");
            SceneManager.LoadScene(EndSceneName, LoadSceneMode.Single);
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(EndSceneName));
            FindObjectOfType<TextScript>().changeText("La maison n'a survécu à la tempête...");
        }
    }
}
