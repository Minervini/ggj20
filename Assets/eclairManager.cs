﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class eclairManager : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public AudioSource audio;
    public Animator animator;
    public Image overlay;

    public void drawEclair(Vector3 strikePoint){
        lineRenderer.enabled = true;
        lineRenderer.SetPosition(0, new Vector3(strikePoint.x, 10));
        lineRenderer.SetPosition(1, new Vector3(strikePoint.x + Random.Range(-2, 2), 10 - Random.Range(2,4)));
        lineRenderer.SetPosition(2, new Vector3(strikePoint.x + Random.Range(-3, 3), 10 - Random.Range(1,5)));
        lineRenderer.SetPosition(3, new Vector3(strikePoint.x + Random.Range(-3, 3), 10 - Random.Range(4,6)));
        lineRenderer.SetPosition(4, strikePoint);

        animator.Play("lightning", -1, 0f);
        audio.Play();
    }

    void StopDrawEclair(){ // called in "lightinig" animation
        lineRenderer.enabled = false;
        overlay.color = new Color(1, 1, 1, 1);
    }
}
