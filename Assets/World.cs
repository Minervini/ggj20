﻿using System;
using System.Collections;
using LitJson;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

public class World : MonoBehaviour
{
    public float delay = 30f;
    public GameObject cow;
    public GameObject ducks;
    public List<GameObject> spawnPoints;

    public GameObject hole;
    public GameObject ground;
    public GameObject roof;

    public GameObject house;

    public GameObject leaf;
    public Vector2 wind;
    public LayerMask ignoreLayerLighting;
    public delegate void OnWindChangedEvent(Vector2 wind);
    public static event OnWindChangedEvent OnWindChanged;

    private World instance;

    private void Awake()
    {
        spawnPoints = new List<GameObject>();
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        //DontDestroyOnLoad(gameObject);
    }

    public void registerSpawner(GameObject spawner) {
        spawnPoints.Add(spawner);
    }

    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine(GetWind());
        StartCoroutine(InvokEvent());
    }

    // Gets the wind at a random (lat;long).
    private IEnumerator GetWind()
    {
        for(; ; )
        {
            int lat = (int)UnityEngine.Random.Range(-90f, 90f);
            int lon = (int)UnityEngine.Random.Range(-180f, 180f);
            string endpoint = "api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon;

            UnityWebRequest request = UnityWebRequest.Get(endpoint + "&appid=e6bd2f001b52c1ca155730b9c22038e8");

            yield return request.SendWebRequest();

            try
            {
                JsonData resp = JsonMapper.ToObject(request.downloadHandler.text);
                JsonData windJson = resp["wind"];

                float speed = float.Parse(windJson["speed"].ToString()) * .5f;
                float degree = float.Parse(windJson["deg"].ToString()) * .5f;

                if (speed == 0)
                {
                    speed = (int)UnityEngine.Random.Range(1f, 10f);
                }
                Vector2 force = new Vector2(Mathf.Sin(degree) * speed, Mathf.Cos(degree) * speed);

                OnWindChanged?.Invoke(force);
                wind = force;
            }
            catch (Exception e)
            {
                Debug.Log(request.responseCode + " - "  + e);
            }
            yield return new WaitForSeconds(delay);
        }
    }

    private IEnumerator InvokEvent ()
    {
        double prob = 0.0;
        for(; ; )
        {
            prob += 0.1;
            double gumble = UnityEngine.Random.Range(0.0f, 1.0f);

            if (gumble < prob)
            {
                if (gumble <= 0.20)
                    spawnLighting();
                else {
                    if (gumble <= 0.4)
                        spawnCow();
                    else
                        spawnDucks();
                }
                prob = 0;
            }
            for(int i = 0; i < 5; i++)
                spawnLeaf();

            yield return new WaitForSeconds(2);
        }
    }

    private void spawnCow() {
        if (spawnPoints.Count > 0)
        {
            GameObject point = spawnPoints[UnityEngine.Random.Range(0, spawnPoints.Count)];
            GameObject cowInstance = Instantiate(cow, point.transform);
            cowInstance.transform.position = point.transform.position;

            float centerX = 0;
            float centerY = roof.transform.position.y;

            int forceX = (int)(centerX -point.transform.position.x) * UnityEngine.Random.Range(300, 750);
            int forceY =  (int)Math.Max(0, centerY - point.transform.position.y) * UnityEngine.Random.Range(500, 1000);

            //Debug.Log("ForceY : " + point.transform.position.y);
            //Debug.Log("Force de la vache : " + forceX + " " + forceY);
            cowInstance.GetComponent<Rigidbody2D>().AddForce(new Vector2(forceX, forceY));
        }
    }

    private void spawnLeaf() {

        if (spawnPoints.Count > 0)
        {
            float x = UnityEngine.Random.Range(-1, 1);
            float y = UnityEngine.Random.Range(-1, 1);
            float z = UnityEngine.Random.Range(-5, 40);
            GameObject point = spawnPoints[UnityEngine.Random.Range(0, spawnPoints.Count)];
            GameObject leafInstance = Instantiate(leaf, point.transform);
            leafInstance.transform.position = point.transform.position + new Vector3(x, y, z);
            leafInstance.GetComponent<leafScript>().wind = wind;
        }
    }

    private void spawnDucks() {

        if (spawnPoints.Count > 0)
        {
            for (int i = 0; i < 5; i++) {
                GameObject point = spawnPoints[UnityEngine.Random.Range(0, spawnPoints.Count)];
                GameObject instance = Instantiate(ducks, point.transform);
                instance.transform.position = point.transform.position;

                float centerX = 0;
                float centerY = roof.transform.position.y;

                int forceX = (int)(centerX -point.transform.position.x) * UnityEngine.Random.Range(200, 500);
                int forceY =  (int)Math.Max(0, centerY - point.transform.position.y) * UnityEngine.Random.Range(300, 600);

                //Debug.Log("ForceY : " + point.transform.position.y);
                //Debug.Log("Force de la vache : " + forceX + " " + forceY);
                instance.GetComponent<Rigidbody2D>().AddForce(new Vector2(forceX, forceY));
            }
        }
    }

    private void spawnLighting() {
        float x = ground.transform.localScale.x;

        x = UnityEngine.Random.Range(-8, 8);
        RaycastHit2D hit = Physics2D.Raycast(new Vector2(x, 20), Vector2.down, 100, ignoreLayerLighting);

        if (hit.collider != null && hit.collider.gameObject.tag == "Roof")
        {
            FindObjectOfType<eclairManager>().drawEclair(hit.point);

            house.GetComponent<House>().RemoveRandomTile();
        }
    }
}
